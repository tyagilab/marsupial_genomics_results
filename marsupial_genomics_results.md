
# Characterisation and comparative analyses of immune genes in marsupials


### ABSTRACT

Marsupials (metatherians) are known for their unique biological features and their position in vertebrate phylogeny. However, it has only been since the sequencing of the first marsupial genome that its value has been more widely recognised. We now have genome sequences for 18 distantly related marsupial species available through DNA Zoo initiative, with the promise of many more genomes to be sequenced in the near future, making this a particularly exciting time in marsupial genomics.
The emergence of transmissible cancer, which is obliterating the Tasmanian devil population, has increased the importance of obtaining and analysing marsupial genome sequence for understanding such diseases as well as for conservation efforts. In addition, these genome sequences have facilitated studies aimed at answering questions regarding gene and genome evolution and provided insight into the evolution of epigenetic mechanisms.
The goal of this project is to develop a containerised workflow solution for map the already characterised 800 genes vital to the immune response in the human genome for the 18 marsupial genomes available now. Among these genes are highly divergent immune genes such as cytokines, natural killer cell receptors, and antimicrobials.
This work will reveal the level of complexity of the marsupial immunome as compared to the human.

---

### Background
Although home to the majority of the world’s marsupials, Australia has a terrible reputation for the extinction of its native species, recording the world’s highest extinction rate for recent mammals. Reasons are many which include increasing habitat fragmentation and predation from introduced species on Australia’s unique marsupial populations, however, the risks posed by emerging diseases are becoming increasingly apparent, with the threat of emerging diseases to wildlife proposed to increase as a result of climate change and human encroachment on wildlife habitat, which could have devastating effects on many already threatened marsupial species. Understanding how the immune response of marsupials compares to that of humans and their eutherian counterparts would help to determine if vaccines or treatments used to control the same or similar disease in other mammalian species are likely to be useful for marsupials.
This project will provide an opportunity to utilise high-performance computing resources to run intense comparative genomics exercise for the marsupial genomes available open-source via www.dnazoo.org

---

### Data sources:

- Immune gene database
- Manual collection of immune genes from marsupial literature
- Assembly information:
    - Macropus eugenni (Tamar wallaby) : https://www.dnazoo.org/assemblies/Macropus_eugenii

---

### Tools:

- Custom Python code
- Unix Shell programming
- Pawsey Supercomputing resources
- R code for Chromomap

---

# Method followed

1. Prepared a list of immune-related genes with key references
    - The list was obtained from the InnateDB database which has a collection of close to 6,000 human genes.
    - This list contains most of the Human immune gene details right from names of the immune genes to where they lie on the chromosomes



```python

```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>species</th>
      <th>taxonId</th>
      <th>ensembl</th>
      <th>entrez</th>
      <th>name</th>
      <th>fullname</th>
      <th>synonym</th>
      <th>signature</th>
      <th>chromStart</th>
      <th>...</th>
      <th>goFunctions</th>
      <th>goLocalizations</th>
      <th>cerebralLocalization</th>
      <th>nrIntxsValidated</th>
      <th>nrIntxsPredicted</th>
      <th>transcripts</th>
      <th>humanOrthologs</th>
      <th>mouseOrthologs</th>
      <th>bovineOrthologs</th>
      <th>lastModified</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>21</td>
      <td>Homo sapiens</td>
      <td>9606</td>
      <td>ENSG00000099715</td>
      <td>27328,83259</td>
      <td>PCDH11Y</td>
      <td>protocadherin 11 Y-linked</td>
      <td>NaN</td>
      <td>PCDH11Y|IDBG-21|ENSG00000099715|27328,83259</td>
      <td>5000226</td>
      <td>...</td>
      <td>GO:0005509: calcium ion binding</td>
      <td>GO:0005509: calcium ion binding</td>
      <td>Plasma membrane</td>
      <td>0</td>
      <td>0</td>
      <td>23|25|29|228206|810827|810829</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2017-02-20</td>
    </tr>
    <tr>
      <td>1</td>
      <td>67</td>
      <td>Homo sapiens</td>
      <td>9606</td>
      <td>ENSG00000092377</td>
      <td>90665</td>
      <td>TBL1Y</td>
      <td>transducin (beta)-like 1, Y-linked</td>
      <td>NaN</td>
      <td>TBL1Y|IDBG-67|ENSG00000092377|90665</td>
      <td>6910686</td>
      <td>...</td>
      <td>GO:0005515: protein binding</td>
      <td>GO:0005515: protein binding</td>
      <td>Nucleus</td>
      <td>3</td>
      <td>0</td>
      <td>69|73|75</td>
      <td>NaN</td>
      <td>Tbl1x|IDBG-157725|ENSMUSG00000025246|21372</td>
      <td>TBL1X|IDBG-639526|ENSBTAG00000018112|100849399...</td>
      <td>2017-02-20</td>
    </tr>
    <tr>
      <td>2</td>
      <td>191</td>
      <td>Homo sapiens</td>
      <td>9606</td>
      <td>ENSG00000114374</td>
      <td>8287</td>
      <td>USP9Y</td>
      <td>ubiquitin specific peptidase 9, Y-linked</td>
      <td>NaN</td>
      <td>USP9Y|IDBG-191|ENSG00000114374|8287</td>
      <td>12701231</td>
      <td>...</td>
      <td>GO:0004843: ubiquitin-specific protease activi...</td>
      <td>GO:0004843: ubiquitin-specific protease activi...</td>
      <td>Cytoplasm</td>
      <td>9</td>
      <td>0</td>
      <td>195|361872</td>
      <td>NaN</td>
      <td>Usp9y|IDBG-127674|ENSMUSG00000069044|107868</td>
      <td>NaN</td>
      <td>2017-02-20</td>
    </tr>
    <tr>
      <td>3</td>
      <td>238</td>
      <td>Homo sapiens</td>
      <td>9606</td>
      <td>ENSG00000165246</td>
      <td>22829</td>
      <td>NLGN4Y</td>
      <td>neuroligin 4, Y-linked</td>
      <td>NaN</td>
      <td>NLGN4Y|IDBG-238|ENSG00000165246|22829</td>
      <td>14522638</td>
      <td>...</td>
      <td>GO:0004872: receptor activity#GO:0005515: prot...</td>
      <td>GO:0004872: receptor activity,GO:0005515: prot...</td>
      <td>Cell surface</td>
      <td>0</td>
      <td>0</td>
      <td>240|242|246|248|250|362070</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2017-02-20</td>
    </tr>
    <tr>
      <td>4</td>
      <td>259</td>
      <td>Homo sapiens</td>
      <td>9606</td>
      <td>ENSG00000101557</td>
      <td>9097</td>
      <td>USP14</td>
      <td>ubiquitin specific peptidase 14 (tRNA-guanine ...</td>
      <td>TGT</td>
      <td>USP14|IDBG-259|ENSG00000101557|9097</td>
      <td>158383</td>
      <td>...</td>
      <td>GO:0004197: cysteine-type endopeptidase activi...</td>
      <td>GO:0004197: cysteine-type endopeptidase activi...</td>
      <td>Cell surface</td>
      <td>70</td>
      <td>4</td>
      <td>261|263|228677|726168|727583|727521|808979</td>
      <td>NaN</td>
      <td>Usp14|IDBG-128322|ENSMUSG00000047879|59025</td>
      <td>BT.20599|IDBG-644085|ENSBTAG00000019214|505106</td>
      <td>2017-02-20</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 26 columns</p>
</div>



---

2. Use this Human immune gene list, search and compare with the Tammar Wallaby genome.
    - Initially, for the development of a single pipeline process, the analysis was performed on genome data of Tammar Wallaby.
    - Custom python code was used to check and find the Human immune genes in the Tammar Wallaby genome
    - Further analysis was performed to get to know the matches better. It was observed that out of the 2435 genes, most of the genes were found on only 8 Scaffolds.
    - It was also observed that eight of the Scaffolds were really large in length.


```python

```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chromosome</th>
      <th>gene_count</th>
      <th>chromosome_length</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>HiC_scaffold_1</td>
      <td>892</td>
      <td>652905989</td>
    </tr>
    <tr>
      <td>109</td>
      <td>HiC_scaffold_2</td>
      <td>764</td>
      <td>438350579</td>
    </tr>
    <tr>
      <td>189</td>
      <td>HiC_scaffold_3</td>
      <td>485</td>
      <td>396059312</td>
    </tr>
    <tr>
      <td>214</td>
      <td>HiC_scaffold_5</td>
      <td>469</td>
      <td>375244499</td>
    </tr>
    <tr>
      <td>203</td>
      <td>HiC_scaffold_4</td>
      <td>410</td>
      <td>386955683</td>
    </tr>
    <tr>
      <td>230</td>
      <td>HiC_scaffold_6</td>
      <td>268</td>
      <td>314480743</td>
    </tr>
    <tr>
      <td>248</td>
      <td>HiC_scaffold_7</td>
      <td>177</td>
      <td>132405058</td>
    </tr>
    <tr>
      <td>265</td>
      <td>HiC_scaffold_8</td>
      <td>90</td>
      <td>72834326</td>
    </tr>
    <tr>
      <td>65</td>
      <td>HiC_scaffold_156</td>
      <td>4</td>
      <td>147989</td>
    </tr>
    <tr>
      <td>278</td>
      <td>HiC_scaffold_9213</td>
      <td>4</td>
      <td>108630</td>
    </tr>
    <tr>
      <td>151</td>
      <td>HiC_scaffold_25534</td>
      <td>3</td>
      <td>106340</td>
    </tr>
    <tr>
      <td>78</td>
      <td>HiC_scaffold_16697</td>
      <td>3</td>
      <td>103894</td>
    </tr>
    <tr>
      <td>79</td>
      <td>HiC_scaffold_17130</td>
      <td>3</td>
      <td>109002</td>
    </tr>
    <tr>
      <td>244</td>
      <td>HiC_scaffold_65</td>
      <td>2</td>
      <td>150411</td>
    </tr>
    <tr>
      <td>97</td>
      <td>HiC_scaffold_18245</td>
      <td>2</td>
      <td>106956</td>
    </tr>
    <tr>
      <td>132</td>
      <td>HiC_scaffold_22904</td>
      <td>2</td>
      <td>103614</td>
    </tr>
    <tr>
      <td>130</td>
      <td>HiC_scaffold_22752</td>
      <td>2</td>
      <td>105468</td>
    </tr>
    <tr>
      <td>127</td>
      <td>HiC_scaffold_21903</td>
      <td>2</td>
      <td>104851</td>
    </tr>
    <tr>
      <td>118</td>
      <td>HiC_scaffold_20937</td>
      <td>2</td>
      <td>105038</td>
    </tr>
    <tr>
      <td>110</td>
      <td>HiC_scaffold_20022</td>
      <td>2</td>
      <td>101964</td>
    </tr>
  </tbody>
</table>
</div>



- A Chromomap was plotted for only these 8 scaffolds. The following segment-annotation plot was obtained.

<img src="./plots/Chromomap_1-8_cropped.png">

---
3. The Segment-annotation of Tammar Wallaby looked very different than usual. Hence a comparative analysis was done. A Chromomap for the human genome was plotted for the same list of immune genes.

<img src="./plots/Segmentation_plot_cropped.png">

---
4. A workflow was then established to look for the same immune genes in 69 mammals whose data was available with DNAZoo.
    - Top 15 species with least immune genes found


```python

```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>species_name</th>
      <th>found_count</th>
      <th>missing_count</th>
      <th>%_found</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>58</td>
      <td>Pseudochirops_cupreus_HiC</td>
      <td>0</td>
      <td>4723</td>
      <td>0.00</td>
    </tr>
    <tr>
      <td>45</td>
      <td>Mesocricetus_auratus</td>
      <td>813</td>
      <td>3910</td>
      <td>17.21</td>
    </tr>
    <tr>
      <td>31</td>
      <td>Peromyscus_crinitus</td>
      <td>855</td>
      <td>3868</td>
      <td>18.10</td>
    </tr>
    <tr>
      <td>16</td>
      <td>Phalanger_gymnotis</td>
      <td>1032</td>
      <td>3691</td>
      <td>21.85</td>
    </tr>
    <tr>
      <td>55</td>
      <td>Procavia_capensis__Pcap_2.0</td>
      <td>1529</td>
      <td>3194</td>
      <td>32.37</td>
    </tr>
    <tr>
      <td>36</td>
      <td>Struthio_camelus</td>
      <td>1647</td>
      <td>3076</td>
      <td>34.87</td>
    </tr>
    <tr>
      <td>64</td>
      <td>Monodelphis_domestica</td>
      <td>1921</td>
      <td>2802</td>
      <td>40.67</td>
    </tr>
    <tr>
      <td>32</td>
      <td>Pteropus_vampyrus</td>
      <td>1986</td>
      <td>2737</td>
      <td>42.05</td>
    </tr>
    <tr>
      <td>20</td>
      <td>Fukomys_damarensis</td>
      <td>2000</td>
      <td>2723</td>
      <td>42.35</td>
    </tr>
    <tr>
      <td>6</td>
      <td>Orycteropus_afer</td>
      <td>2008</td>
      <td>2715</td>
      <td>42.52</td>
    </tr>
    <tr>
      <td>54</td>
      <td>Castor_canadensis</td>
      <td>2052</td>
      <td>2671</td>
      <td>43.45</td>
    </tr>
    <tr>
      <td>44</td>
      <td>Cavia_porcellus</td>
      <td>2101</td>
      <td>2622</td>
      <td>44.48</td>
    </tr>
    <tr>
      <td>30</td>
      <td>Coendou_prehensilis</td>
      <td>2115</td>
      <td>2608</td>
      <td>44.78</td>
    </tr>
    <tr>
      <td>63</td>
      <td>Eidolon_helvum</td>
      <td>2366</td>
      <td>2357</td>
      <td>50.10</td>
    </tr>
    <tr>
      <td>48</td>
      <td>Tursiops_truncatus</td>
      <td>2391</td>
      <td>2332</td>
      <td>50.62</td>
    </tr>
  </tbody>
</table>
</div>



- Top 15 most immune genes found in mammals


```python

```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>species_name</th>
      <th>found_count</th>
      <th>missing_count</th>
      <th>%_found</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>21</td>
      <td>Saimiri_boliviensis</td>
      <td>3151</td>
      <td>1572</td>
      <td>66.72</td>
    </tr>
    <tr>
      <td>33</td>
      <td>Eulemur_flavifrons</td>
      <td>3133</td>
      <td>1590</td>
      <td>66.33</td>
    </tr>
    <tr>
      <td>10</td>
      <td>Axis_porcinus</td>
      <td>3110</td>
      <td>1613</td>
      <td>65.85</td>
    </tr>
    <tr>
      <td>50</td>
      <td>Microcebus_murinus</td>
      <td>3109</td>
      <td>1614</td>
      <td>65.83</td>
    </tr>
    <tr>
      <td>17</td>
      <td>Ceratotherium_simum</td>
      <td>3109</td>
      <td>1614</td>
      <td>65.83</td>
    </tr>
    <tr>
      <td>3</td>
      <td>Equus_asinus__ASM303372v1</td>
      <td>3082</td>
      <td>1641</td>
      <td>65.26</td>
    </tr>
    <tr>
      <td>26</td>
      <td>Odobenus_rosmarus</td>
      <td>3081</td>
      <td>1642</td>
      <td>65.23</td>
    </tr>
    <tr>
      <td>4</td>
      <td>Ursus_arctos</td>
      <td>3075</td>
      <td>1648</td>
      <td>65.11</td>
    </tr>
    <tr>
      <td>40</td>
      <td>Phoca_vitulina</td>
      <td>3073</td>
      <td>1650</td>
      <td>65.06</td>
    </tr>
    <tr>
      <td>42</td>
      <td>Canis_lupus_familiaris</td>
      <td>3060</td>
      <td>1663</td>
      <td>64.79</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Neofelis_nebulosa</td>
      <td>3055</td>
      <td>1668</td>
      <td>64.68</td>
    </tr>
    <tr>
      <td>38</td>
      <td>Hippotragus_niger</td>
      <td>3053</td>
      <td>1670</td>
      <td>64.64</td>
    </tr>
    <tr>
      <td>35</td>
      <td>Mirounga_angustirostris</td>
      <td>3052</td>
      <td>1671</td>
      <td>64.62</td>
    </tr>
    <tr>
      <td>59</td>
      <td>Propithecus_coquereli</td>
      <td>3046</td>
      <td>1677</td>
      <td>64.49</td>
    </tr>
    <tr>
      <td>57</td>
      <td>Odocoileus_virginianus</td>
      <td>3043</td>
      <td>1680</td>
      <td>64.43</td>
    </tr>
  </tbody>
</table>
</div>



- It was observed that most of the marsupials had a higher percentage of immune genes found in them (around to 40-60% found)


```python

```


![png](output_13_0.png)


---

5. LAST Alignment extraction of Protein genes with chromosome sequences for Tammar Wallaby
    - The protein genes were searched and matched with the LAST Alignment file and alignment details such as the alignment score, mismap value and alignment sequences were extracted.
    - There were multiple occurrences of the same gene presence on same or different scaffolds.


```python

```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gene_name</th>
      <th>match_score</th>
      <th>match_mismap</th>
      <th>gene_alignment</th>
      <th>chromosome_alignment</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>USP14</td>
      <td>4211</td>
      <td>1e-09</td>
      <td>ATGCCGCTCTACTCCGTTACTGTAAAATGGGGAAAGGAGAAATTTG...</td>
      <td>ATGCCGCTCTTCTCAGTTAATGTAAAATGGGGAAAAGAGAAATTTG...</td>
    </tr>
    <tr>
      <td>3</td>
      <td>THOC1</td>
      <td>208</td>
      <td>0.83</td>
      <td>ATGTCTCCGACGCCGCCGCTCTTCAGTTTGCCCGAAGCGCGGACGC...</td>
      <td>ATGTCTCCGTCGCCGCCTCTCTTCAGTTTCCCCGAAGCACAGACTC...</td>
    </tr>
    <tr>
      <td>21</td>
      <td>COLEC12</td>
      <td>235</td>
      <td>0.0008</td>
      <td>AGACGACTTCGCAGAGgaggaggaggTGCAATCCTTCGGTTACAAG...</td>
      <td>AGACGATTTTGCAGAGGAGGACGAGGTGCAGTCCTTCGGTTACAAG...</td>
    </tr>
    <tr>
      <td>29</td>
      <td>ADCYAP1</td>
      <td>379</td>
      <td>1e-09</td>
      <td>TctctctctgcgcccccttctctcCGTGTCACGCTCCCTCCTGGTT...</td>
      <td>TTCCTCTCTGCCCCCTCTTCTTTCAGTGTCACACGccctcttagtt...</td>
    </tr>
    <tr>
      <td>33</td>
      <td>RALBP1</td>
      <td>979</td>
      <td>1e-09</td>
      <td>ATGACTGAGTGCTTCCTGCCCCCCACCAGCAGCCCCAGTGAACACC...</td>
      <td>ATGACTGAGTGCTTCCTGCCTCCTACCAGCAGTCCCAGTGAACACC...</td>
    </tr>
    <tr>
      <td>42</td>
      <td>CXADR</td>
      <td>424</td>
      <td>0.63</td>
      <td>TGGATTTCGCCAGAAGTTTGAGTATCACTACTCCTGAAGAGATGAT...</td>
      <td>TAGATGTGACCAGAAGTATTACTATAACCACTACTGAAGAGAAGAT...</td>
    </tr>
    <tr>
      <td>45</td>
      <td>CECR2</td>
      <td>487</td>
      <td>1.0</td>
      <td>ATGTGCCCAGAGGAGGGCG---------GCgcggccgggctgggcG...</td>
      <td>ATGTgcccggaggagggcgcgggcgaggacgcggCGGGACTGGGCG...</td>
    </tr>
    <tr>
      <td>61</td>
      <td>BCL2L13</td>
      <td>263</td>
      <td>1.0</td>
      <td>CCGCCCCGACGCCGGCCGTGACGAAGGCACGCCGGGGTGACCTCAC...</td>
      <td>CCGCCTAGACGCTAGCGCTGACGTCACCACGCACGGGTGACCTCCG...</td>
    </tr>
    <tr>
      <td>68</td>
      <td>JAM2</td>
      <td>224</td>
      <td>0.25</td>
      <td>ATCATAAGGCCTATGGGTTTTCTGCCCCAAAAGACCAACAAGTAGT...</td>
      <td>ATCACAATGCATATGGATTCTCTACCCCAAAAAACCATCAAGTTGT...</td>
    </tr>
    <tr>
      <td>75</td>
      <td>APP</td>
      <td>250</td>
      <td>1.0</td>
      <td>GATGAGCTGCTTCAGAAAGAGCAA-----AACTATTCAGATGACGT...</td>
      <td>GATGAGTTGCTTCAGAAAACaaaacaaaAAGCTACTCAGATGATGT...</td>
    </tr>
    <tr>
      <td>77</td>
      <td>PSMG2</td>
      <td>688</td>
      <td>0.5</td>
      <td>GCCAGCAGTATCTGTTGGAAATGTTGGCCAGCTTGCAATGGATCTG...</td>
      <td>GCCAGCAGTATCTGTGGGGAATGTTGGGCAGCTTGCAATAGATCTG...</td>
    </tr>
    <tr>
      <td>87</td>
      <td>ADAMTS1</td>
      <td>686</td>
      <td>1e-09</td>
      <td>gccgcggcgCTACTGGCCGTGTCGGACGCACTCGGGCGCCCCTCCG...</td>
      <td>GCAGTGGTTTTCCTGACTATGCCAGGCGTACACTGTCTCCCCGTGG...</td>
    </tr>
    <tr>
      <td>95</td>
      <td>CLDN17</td>
      <td>1819</td>
      <td>1e-09</td>
      <td>ATGGCATTTTATCCCTTGCAAATTGCTGGGCTGGTTCTTGGGTTCC...</td>
      <td>ATGGCTTTGTATCCCCTGCAGATTGCTGGACTGGTTCTCGGATTCT...</td>
    </tr>
    <tr>
      <td>96</td>
      <td>TBX1</td>
      <td>887</td>
      <td>1.0</td>
      <td>AGACGCGGCTGAGGCCCGGCGAGAATTCCAGCGCGACGCGGGCGGG...</td>
      <td>AGACGCTGCTGAGAGCCGGCGGGAGTACGAGCGAGAAGCCAGCGGG...</td>
    </tr>
    <tr>
      <td>103</td>
      <td>CLDN8</td>
      <td>2023</td>
      <td>1e-09</td>
      <td>ATGGCAACCCATGCCTTAGAAATCGCTGGGCTGTTTCTTGGTGGTG...</td>
      <td>ATGGCTTACTATGGTCTGCAAATAGTCGGACTAATCCTAGGAGGAA...</td>
    </tr>
  </tbody>
</table>
</div>



---

6. Another workflow was established to extract the LAST alignments of the rest of the mammals to the human genome
    - All the alignment details were extracted for all species based on the protein gene matches
    - Post filtering out the genes based on the cut-off of 0.05 for the mismap values, the following plot was obtained to get the count of genes

<img src="./plots/all_species_found.png">

---

7. Using the gene list from the step 1, comparison of the genomes of a range of mammals was performed to identify unique and shared immunity related-genes.
    - Expected outcome is a table of Gene X organisms, 0 and 1 to represent presence and absence of genes


```python

```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gene_name</th>
      <th>Macropus_giganteus</th>
      <th>Macropus_fuliginosus</th>
      <th>Pseudocheirus_occidentalis</th>
      <th>Peromyscus_crinitus</th>
      <th>Propithecus_coquereli</th>
      <th>Pseudochirops_corinnae</th>
      <th>Ceratotherium_simum</th>
      <th>Enhydra_lutris</th>
      <th>Saimiri_boliviensis</th>
      <th>...</th>
      <th>Macropus_eugenii</th>
      <th>Ailurus_fulgens</th>
      <th>Panthera_tigris</th>
      <th>Monodelphis_domestica</th>
      <th>Choloepus_hoffmanni</th>
      <th>Odobenus_rosmarus</th>
      <th>Odocoileus_virginianus</th>
      <th>Procyon_lotor</th>
      <th>Phalanger_gymnotis</th>
      <th>Lagenorhynchus_obliquidens</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>EMILIN1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>1</td>
      <td>ACOX1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>2</td>
      <td>FCER1G</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>3</td>
      <td>POLR2G</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>4</td>
      <td>DBH</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>5</td>
      <td>MRPS30</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>6</td>
      <td>IL10</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>7</td>
      <td>MYO18A</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>8</td>
      <td>CMTM6</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>9</td>
      <td>NINJ1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>10</td>
      <td>TONSL</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <td>11</td>
      <td>EXTL1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>12</td>
      <td>ZC3HC1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>13</td>
      <td>SNX17</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>14</td>
      <td>JAKMIP2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>15 rows × 24 columns</p>
</div>



- A similar table with match scores instead of just 0 and 1 was also generated to get a higher understanding of the level of matches for genes in the species.



```python

```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gene_name</th>
      <th>Macropus_giganteus</th>
      <th>Macropus_fuliginosus</th>
      <th>Pseudocheirus_occidentalis</th>
      <th>Peromyscus_crinitus</th>
      <th>Propithecus_coquereli</th>
      <th>Pseudochirops_corinnae</th>
      <th>Ceratotherium_simum</th>
      <th>Enhydra_lutris</th>
      <th>Saimiri_boliviensis</th>
      <th>...</th>
      <th>Macropus_eugenii</th>
      <th>Ailurus_fulgens</th>
      <th>Panthera_tigris</th>
      <th>Monodelphis_domestica</th>
      <th>Choloepus_hoffmanni</th>
      <th>Odobenus_rosmarus</th>
      <th>Odocoileus_virginianus</th>
      <th>Procyon_lotor</th>
      <th>Phalanger_gymnotis</th>
      <th>Lagenorhynchus_obliquidens</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>DNM1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>301</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>1</td>
      <td>PTGER3</td>
      <td>926</td>
      <td>928</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>870</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>908</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>2</td>
      <td>SLFN14</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>4522</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>3</td>
      <td>MASP2</td>
      <td>1374</td>
      <td>1397</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1175</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1350</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>4</td>
      <td>PPP2CB</td>
      <td>3011</td>
      <td>3159</td>
      <td>0</td>
      <td>505</td>
      <td>0</td>
      <td>2185</td>
      <td>996</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>5</td>
      <td>AKAP13</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>301</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>6</td>
      <td>AIMP2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1499</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>7</td>
      <td>CSF2RB</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2533</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>8</td>
      <td>IGF2R</td>
      <td>0</td>
      <td>856</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>781</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>841</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>9</td>
      <td>DNAJC5</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>947</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>576</td>
    </tr>
    <tr>
      <td>10</td>
      <td>CDK19</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1201</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>11</td>
      <td>ID1</td>
      <td>1345</td>
      <td>1351</td>
      <td>0</td>
      <td>1563</td>
      <td>0</td>
      <td>1237</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1294</td>
      <td>1736</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1205</td>
      <td>0</td>
    </tr>
    <tr>
      <td>12</td>
      <td>ATP2A1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>3141</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>13</td>
      <td>CPA3</td>
      <td>619</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>14</td>
      <td>ERBB3</td>
      <td>1601</td>
      <td>1676</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1530</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1665</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2342</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>15 rows × 24 columns</p>
</div>



- A Heatmap was plotted for the most found immune genes. The match score was used for change in density. It was observed that mostly 5 species had the genes present on them predominantly.


```python

```


![png](output_21_0.png)


---

7. Phylogenetic analyses to identify evolutionary novelties in immune modulation components and form strong hypotheses as to when key pathways have evolved or diverged in these mammals. This will serve as a key resource for future immunology research in mammals.

- A phylogenetic tree was demonstrated using an immunoglobin gene IGDCC3

<img src="./plots/Phylogenetic_tree_IGDCC3.png">


- A workflow was established to generate fasta files extracting the alignments of species for all the immune genes. This can further be used to plot and investigate phylogenetic tree for a particular immune gene.
